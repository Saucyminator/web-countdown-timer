---
---

'use strict';

// Credits: https://codepen.io/javanigus/pen/KrMRvd

$(document).ready(function () {
  var endOfTimerText = `Ha ett härligt sommarlov!`;
  var timeElement = document.querySelector("#datetime");
  var textElement = document.querySelector("#text");
  var targetTime = moment.tz("2024-06-12T10:50:00", "Europe/Stockholm"); // calculate difference between two times
  var currentTime = moment.tz(); // based on time set in user's computer time / OS
  var duration = moment.duration(targetTime.diff(currentTime)); // get duration between two times
  var intervalMs = 1000; // 1sec

  // loop to countdown every 1 second
  var intervalId = setInterval(setText, intervalMs);

  function setText () {
    // get updated duration
    duration = moment.duration(duration - intervalMs, 'milliseconds');

    if (duration.asSeconds() <= 0) {
      clearInterval(intervalId);

      timeElement.classList.add("hidden");

      textElement.classList.add("shown");
      textElement.innerText = endOfTimerText;
    } else {
      var text = ``;

      if (duration.days() > 0) {
        text += `${duration.days()} ${duration.days() > 1 ? `dagar`: `dag` }, `;
      }
      if (duration.hours() > 0) {
        text += `${duration.hours()} ${duration.hours() > 1 ? `timmar`: `timme` }, `;
      }
      if (duration.minutes() > 0) {
        text += `${duration.minutes()} min, `;
      }

      text += `${duration.seconds()} sek`;

      timeElement.innerText = text;
    }
  }
});
