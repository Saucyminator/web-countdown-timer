# Countdown timer
Displays a simple countdown timer


## Development (locally using Docker)
1. Fork, clone or download this project  
2. Download and setup Docker Desktop from [docker.com][]  
3. Download and setup VS Code from [code.visualstudio.com][]  
  a. Install both the "Docker" and "Dev Containers" plugins  
4. Open the folder with VSCode and a popup should be shown of folder containing a Dev Container configuration file. Choose "**Reopen in Container**"  
  a. ![](src/images/docs/vscode_dev_container_popup.png)  
5. Run the following commands:  
  a. ```gem update --system```  
  a. ```bundle install```  
6. Command to access the website: ```bundle exec jekyll serve --force_polling --livereload```  
  a. Open website locally at http://127.0.0.1:4000/  

Read more at Jekyll's [documentation][] and more information is available at GitLab's [Jekyll pages][].


## License
[MIT License][]


[docker.com]: https://www.docker.com/
[code.visualstudio.com]: https://code.visualstudio.com/
[documentation]: https://jekyllrb.com/docs/home/
[jekyll pages]: https://gitlab.com/pages/jekyll
[mit license]: LICENSE.md